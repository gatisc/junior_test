<?php
if (!empty($_GET['f']))
{
	switch ($_GET['f']) 
	{
		case 'add_page':
			add_page();
			break;
		case 'display_products':
			display_products();
			break;
	}
}

function add_page()
{
	echo '<form action="/svn/junior_test/add.php">
		  <input type="submit" value="Save" style="float: right">
		  <h1>Product add</h1>
		  <hr>
		  SKU:<br>
		  <input type="text" name="SKU"><br>
		  Name:<br>
		  <input type="text" name="name"><br>
		  Price:<br>
		  <input type="number" name="price"><br><br>
		  <select name="type" onchange="attr_change()" id="type_select">
			  <option value="3">DVD-disc</option>
			  <option value="2">Book</option>
			  <option value="1">Furniture</option>
		  </select><br><br>
		  <span id="attribute_type">Please​ ​provide​ size in Mb</span>:<br>
		  <input type="text" name="attribute_value"><br>
		  <script>
				function attr_change()
				{
					var sel = document.getElementById("type_select");
					
					if (sel.value == 1)
					{
						document.getElementById("attribute_type").innerHTML = "Please​ ​provide​ ​dimensions​ ​in​ ​HxWxL​ ​format";
					}
					else if (sel.value == 2)
					{
						document.getElementById("attribute_type").innerHTML = "Please​ ​provide​ product weight in Kg";
					}
					else if (sel.value == 3)
					{
						document.getElementById("attribute_type").innerHTML = "Please​ ​provide​ size in Mb";
					}
				}
		  </script>
		  </form>';
}

function display_products()
{
	$servername = "localhost";
	$username = "root";
	$password = "root";
	$dbname = "junior_test";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	} 

	$sql = "SELECT product.ID, product.SKU, product.name, product.price, product.attribute_value, product_types.attribute_type, product_types.measurement FROM product INNER JOIN product_types ON product.type=product_types.id;";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		// output data of each row
		echo '<form action="/svn/junior_test/delete.php" style="width: 100%">
		<input type="submit" value="Apply" style="float: right">
		    <select style="float: right; margin-right: 15px;">
			  <option value="volvo">Mass delete action</option>
			</select>';
		echo "<h1>Product list</h1>";
		echo "<hr>";
		
		$counter = 0;
		
		echo "<table style='width:100%;border-spacing: 50px; line-height: 30px;'>";
		echo "<tr>";
		
		while($row = $result->fetch_assoc()) {
			$counter++;
			
			echo "<td  style='border: 1px solid black; text-align: center; padding: 15px; height: 150px'>";
			
			echo "<input type='checkbox' style='float: left' name='products[]' value=".$row['ID'].">";
			
			echo $row["SKU"]. "<br>" . $row["name"]. "<br>" . $row["price"]. " $<br>" . $row["attribute_type"]. ": " . $row["attribute_value"]. " ". $row["measurement"];
			
			echo "</td>";
			if ($counter == 4)
			{
				echo "</tr>";
				echo "<tr>";
				$counter = 0;
			}
		}
		
		echo "</table>";
	} else {
		echo "0 results";
	}
	
	echo "</form>";
	
	$conn->close();
}